package fall2015.cs160.commutercronny;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final String[] fnames = {"speeding", "sharpturn", "suddenstop", "sleepy", "highacc"};

    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private static int notificationId = 0;

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = new Intent(MainActivity.this, LoadingActivity.class);
        startActivity(i);

        generateFakeData();

        setContentView(R.layout.activity_main);


        mNavItems.add(new NavItem("Homepage", "View driving history", R.drawable.test));
        mNavItems.add(new NavItem("Preferences", "Change your preferences", R.drawable.test));
        mNavItems.add(new NavItem("About", "Get to know about us", R.drawable.test));

        // DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // More info: http://codetheory.in/difference-between-setdisplayhomeasupenabled-sethomebuttonenabled-and-setdisplayshowhomeenabled/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.mainContent, new ProfileFragment(), ProfileFragment.TAG)
                .commit();

        Intent intent = new Intent(this, CarService.class);
        IntentFilter filter = new IntentFilter(CarService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

//    // Called when invalidateOptionsMenu() is invoked
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        // If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_search).setVisible(!drawerOpen);
//        return super.onPrepareOptionsMenu(menu);
//    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    /*
* Called when a particular item from the navigation drawer
* is selected.
* */
    private void selectItemFromDrawer(int position) {
        Fragment fragment;

        switch(position) {
            case 1:
                fragment = new PreferencesFragment();
                break;
            case 2:
                fragment = new AboutFragment();
                break;
            default:
                fragment = new ProfileFragment();
                break;
        }

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.mainContent, fragment)
                .commit();

        mDrawerList.setItemChecked(position, true);
        setTitle(mNavItems.get(position).mTitle);

        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    class NavItem {
        String mTitle;
        String mSubtitle;
        int mIcon;

        public NavItem(String title, String subtitle, int icon) {
            mTitle = title;
            mSubtitle = subtitle;
            mIcon = icon;
        }
    }

    class DrawerListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<NavItem> mNavItems;

        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
            mContext = context;
            mNavItems = navItems;
        }

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.drawer_item, null);
            } else {
                view = convertView;
            }

            TextView titleView = (TextView) view.findViewById(R.id.title);
            TextView subtitleView = (TextView) view.findViewById(R.id.subTitle);
            ImageView iconView = (ImageView) view.findViewById(R.id.icon);

            titleView.setText(mNavItems.get(position).mTitle);
            subtitleView.setText(mNavItems.get(position).mSubtitle);
            iconView.setImageResource(mNavItems.get(position).mIcon);

            return view;
        }
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            long time = intent.getLongExtra("time", -1);
            String message = "hey it worked!";
            System.out.println("I GOTS A " + type + " INTENT!!");
            if(type.equals(CarService.SPEEDING)) {
                double speed = intent.getDoubleExtra("speed", -1);
                int limit = intent.getIntExtra("limit", -1);
                try {
                    FileOutputStream fos = openFileOutput(fnames[0], Context.MODE_APPEND);
                    String towrite = time + " " + speed + " " + limit + " " + limit + "\n";
                    fos.write(towrite.getBytes());
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ProfileFragment fragment = (ProfileFragment) getFragmentManager().findFragmentByTag(ProfileFragment.TAG);
                fragment.addSpeedPoint(time, speed, limit, limit);
                message = "You went " + speed + "mph in a " + limit + "mph zone!!";
            } else if(type.equals(CarService.TURNING)) {
                try {
                    FileOutputStream fos = openFileOutput(fnames[1], Context.MODE_APPEND);
                    String towrite = time + "\n";
                    fos.write(towrite.getBytes());
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ProfileFragment fragment = (ProfileFragment) getFragmentManager().findFragmentByTag(ProfileFragment.TAG);
                fragment.addSharpTurn(time);
                message = "What kind of turn was that?";
            } else if(type.equals(CarService.STOPPING)) {
                double intensity = intent.getDoubleExtra("intensity", -1);
                try {
                    FileOutputStream fos = openFileOutput(fnames[2], Context.MODE_APPEND);
                    String towrite = time + " " + intensity + "\n";
                    fos.write(towrite.getBytes());
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ProfileFragment fragment = (ProfileFragment) getFragmentManager().findFragmentByTag(ProfileFragment.TAG);
                fragment.addSuddenStop(time, intensity);
                message = "WOAH THERE, no need to slam those breaks...";
            } else if(type.equals(CarService.ACCELERATING)) {
                double intensity = intent.getDoubleExtra("intensity", -1);
                try {
                    FileOutputStream fos = openFileOutput(fnames[4], Context.MODE_APPEND);
                    String towrite = time + " " + intensity + "\n";
                    fos.write(towrite.getBytes());
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ProfileFragment fragment = (ProfileFragment) getFragmentManager().findFragmentByTag(ProfileFragment.TAG);
                fragment.addHighAcc(time, intensity);
                message = "HOLD YOUR HORSES, that was some acceleration";
            }

            notificationId += 1;
            Intent viewIntent = new Intent(getBaseContext(), MainActivity.class);
            PendingIntent viewPendingIntent = PendingIntent.getActivity(getBaseContext(), 0, viewIntent, 0);
            Date tm = new Date(time);
            Bitmap notification = BitmapFactory.decodeResource(getResources(), R.drawable.notification);
            Bitmap bkgd = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888);
            bkgd.eraseColor(0x3B5998);
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(getBaseContext())
                            .setSmallIcon(R.drawable.notification)
                            .setLargeIcon(notification)
                            .setContentTitle(type)
                            .setContentText(tm.toString())
                            .setColor(0x3B5998)
                            .setSubText(message)
                            .setAutoCancel(true)
                            .extend(new NotificationCompat.WearableExtender().setBackground(bkgd))
                            .setContentIntent(viewPendingIntent);
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(getBaseContext());

            notificationManager.notify(notificationId, notificationBuilder.build());
        }
    };

    public void generateFakeData() {
        long offset = ((long)1000)*60*60*24 + 5000000;
        long time = System.currentTimeMillis() - offset*10;
        try {
            FileOutputStream fos = openFileOutput(fnames[0], Context.MODE_PRIVATE);
            String speed = time + " 80 75 45\n";
            fos.write(speed.getBytes());
            speed = (time + offset) + " 70 60 55\n";
            fos.write(speed.getBytes());
            speed = (time + offset*2)  + " 85 80 60\n";
            fos.write(speed.getBytes());
            speed = (time + offset*3) + " 50 40 45\n";
            fos.write(speed.getBytes());
            speed = (time + offset*4)  + " 45 30 30\n";
            fos.write(speed.getBytes());
            speed = (time + offset*5)  + " 55 50 40\n";
            fos.write(speed.getBytes());
            speed = (time + offset*6)  + " 75 30 50\n";
            fos.write(speed.getBytes());
            speed = (time + offset*7)  + " 85 60 60\n";
            fos.write(speed.getBytes());
            speed = (time + offset*8)  + " 105 60 90\n";
            fos.write(speed.getBytes());
            speed = (time + offset*9)  + " 65 50 40\n";
            fos.write(speed.getBytes());
            fos.close();

            fos = openFileOutput(fnames[1], Context.MODE_PRIVATE);
            String turn = time + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*2) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*2) + "\n";
            fos.write(turn.getBytes());
            fos.close();

            fos = openFileOutput(fnames[2], Context.MODE_PRIVATE);
            String stop = time + " 8\n";
            fos.write(stop.getBytes());
            stop = (time + offset) + " 5.2\n";
            fos.write(stop.getBytes());
            stop = (time + offset*2) + " 2.9\n";
            fos.write(stop.getBytes());
            stop = (time + offset*3) + " 10\n";
            fos.write(stop.getBytes());
            fos.close();

            fos = openFileOutput(fnames[3], Context.MODE_PRIVATE);
            String sleep = time + "\n";
            fos.write(sleep.getBytes());
            turn = (time + offset) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*2) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*2) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*4) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*4) + "\n";
            fos.write(turn.getBytes());
            turn = (time + offset*5) + "\n";
            fos.write(turn.getBytes());
            fos.close();

            fos = openFileOutput(fnames[4], Context.MODE_PRIVATE);
            String acc = time + " 10\n";
            fos.write(acc.getBytes());
            acc = (time + offset) + " 3.3\n";
            fos.write(acc.getBytes());
            acc = (time + offset*2) + " 6\n";
            fos.write(acc.getBytes());
            acc = (time + offset*3) + " 8.2\n";
            fos.write(acc.getBytes());
            acc = (time + offset*4) + " 1.1\n";
            fos.write(acc.getBytes());
            acc = (time + offset*5) + " 5.5\n";
            fos.write(acc.getBytes());
            acc = (time + offset*6) + " 7.0\n";
            fos.write(acc.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
