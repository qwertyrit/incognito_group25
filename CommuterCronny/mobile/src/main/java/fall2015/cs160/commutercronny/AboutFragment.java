package fall2015.cs160.commutercronny;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class AboutFragment extends Fragment {


    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        ImageView mImageView;
//        mImageView = (ImageView) getView().findViewById(R.id.image_view);
//        mImageView.setImageResource(R.drawable.speeding);

        return inflater.inflate(R.layout.fragment_about, container, false);
    }


}
