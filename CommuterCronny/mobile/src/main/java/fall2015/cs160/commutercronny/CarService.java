package fall2015.cs160.commutercronny;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.*;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;


public class CarService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        MessageApi.MessageListener {

    public static final String ACTION = "com.fall2015.cs160.commutercronny.carservice";
    public static final String SPEEDING = "Speeding!";
    public static final String STOPPING = "Stopped suddenly";
    public static final String SLEEPING = "Sleeping?";
    public static final String TURNING = "Sharp turn";
    public static final String ACCELERATING = "High Acceleration";
    private static final double MPS_TO_MPH = 2.23694;
    public static final int UPDATE_INTERVAL_MS = 1500;
    public static final int FASTEST_INTERVAL_MS = 0;
    public static final int SPEED_LIMIT = 5;
    public static final int MASSIVE_SPEED_DIFFERENCE = 1;

    private GoogleApiClient mGoogleApiClient;
    private LocalBroadcastManager broadcaster;
    private LocHandler handler;
    private Location lastLoc;
    private long lastTime;
    private double lastSpeed;

    public CarService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("LocServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        Looper threadLooper = thread.getLooper();
        handler = new LocHandler(threadLooper);
        broadcaster = LocalBroadcastManager.getInstance(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = handler.obtainMessage();
        msg.arg1 = startId;
        handler.sendMessage(msg);
        mGoogleApiClient.connect();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void onConnected(Bundle bundle) {
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("Loc Request Received");
        if(lastLoc == null) {
            lastLoc = location;
            lastTime = System.currentTimeMillis();
            return;
        }

        long currTime = System.currentTimeMillis();
        float[] res = new float[1];
        Location.distanceBetween(lastLoc.getLatitude(), lastLoc.getLongitude(), location.getLatitude(), location.getLongitude(), res);
        double speed = res[0] / (currTime - lastTime) * MPS_TO_MPH;
        if(speed > SPEED_LIMIT) {
            Intent in = new Intent(ACTION);
            in.putExtra("type", SPEEDING);
            in.putExtra("speed", speed);
            in.putExtra("time", currTime);
            in.putExtra("limit", SPEED_LIMIT);
            broadcaster.sendBroadcast(in);
        }
        if(lastSpeed - speed > MASSIVE_SPEED_DIFFERENCE) {
            Intent in = new Intent(ACTION);
            in.putExtra("type", STOPPING);
            in.putExtra("intensity", lastSpeed - speed);
            in.putExtra("time", currTime);
            broadcaster.sendBroadcast(in);
        }
        if(speed - lastSpeed > MASSIVE_SPEED_DIFFERENCE) {
            Intent in = new Intent(ACTION);
            in.putExtra("type", ACCELERATING);
            in.putExtra("intensity", speed - lastSpeed);
            in.putExtra("time", currTime);
            broadcaster.sendBroadcast(in);
        }

        lastLoc = location;
        lastTime = currTime;
        lastSpeed = speed;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private void askLocation() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setExpirationDuration(100);
        locationRequest.setInterval(1000);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        System.out.println("Message Received");

        Intent in = new Intent(ACTION);
        in.putExtra("type", TURNING);
        in.putExtra("time", System.currentTimeMillis());
        broadcaster.sendBroadcast(in);
    }

    private final class LocHandler extends Handler {
        public LocHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            this.postDelayed(new Runnable() {
                @Override
                public void run() {
                    System.out.println("Loc Request Sent");
                    if (mGoogleApiClient.isConnected()) {
                        askLocation();
                    }
                    handler.postDelayed(this, UPDATE_INTERVAL_MS);
                }
            }, FASTEST_INTERVAL_MS);
            stopSelf(msg.arg1);
        }
    }
}