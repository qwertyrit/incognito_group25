package fall2015.cs160.commutercronny;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;

public class ProfileFragment extends Fragment implements
        CompoundButton.OnCheckedChangeListener,
        View.OnClickListener {

    public static String TAG = ProfileFragment.class.getSimpleName();
    private static final long MS_PER_DAY = 60 * 60 * 24 * 1000;

    GraphView graph;
    LineGraphSeries<DataPoint> speedLine; //x = what day (including time) vs y = what the sp1ed was
    LineGraphSeries<DataPoint> trafficLine;
    LineGraphSeries<DataPoint> limitLine;
    int numSpeeds = 1;
    BarGraphSeries<DataPoint> sharpTurnsBar; //total number of turns during each day (x = the day)
    int numTurns = 1;
    PointsGraphSeries<DataPoint> suddenStopPoints; // time in day VS sum of every intensity (x = time in day)
    int numStops = 1;
    LineGraphSeries<DataPoint> sleepyLine; //number of times caught sleeping each day (x = the day)
    int numSleeps = 1;
    BarGraphSeries<DataPoint> highAccBar; //time in day VS average intensity during that time (x = time in day)
    int numAccs = 1;

    ArrayList<DataPoint> stopData = new ArrayList<DataPoint>();
    ArrayList<DataPoint> turnData = new ArrayList<DataPoint>();
    ArrayList<DataPoint> sleepData = new ArrayList<DataPoint>();
    ArrayList<DataPoint> accData = new ArrayList<DataPoint>();
    Hashtable<Long, DataPoint> turnHash = new Hashtable<Long, DataPoint>();
    Hashtable<Long, DataPoint> sleepHash = new Hashtable<Long, DataPoint>();
    Hashtable<Long, DataPoint> accHash = new Hashtable<Long, DataPoint>();

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fview = inflater.inflate(R.layout.fragment_profile, container, false);

        ((Switch)fview.findViewById(R.id.speed)).setOnCheckedChangeListener(this);
        ((Switch)fview.findViewById(R.id.traffic)).setOnCheckedChangeListener(this);
        ((Switch)fview.findViewById(R.id.limit)).setOnCheckedChangeListener(this);
        fview.findViewById(R.id.sharpTurnWrapper).setOnClickListener(this);
        fview.findViewById(R.id.suddenStopWrapper).setOnClickListener(this);
        fview.findViewById(R.id.sleepyConditionWrapper).setOnClickListener(this);
        fview.findViewById(R.id.highAccelerationWrapper).setOnClickListener(this);
        fview.findViewById(R.id.reset).setOnClickListener(this);

        BufferedReader speedFile, turnFile, stopFile, sleepFile, accFile;
        ArrayList<DataPoint> speedData = new ArrayList<DataPoint>();
        ArrayList<DataPoint> trafficData = new ArrayList<DataPoint>();
        ArrayList<DataPoint> limitData = new ArrayList<DataPoint>();

        for(long i = 0; i < 24; i++){
            DataPoint dp = new DataPoint(i, 0);
            accHash.put(i, dp);
            accData.add(dp);
        }

        try {
            speedFile = new BufferedReader(new InputStreamReader(getActivity().openFileInput(MainActivity.fnames[0])));
            String infraction;
            while ((infraction = speedFile.readLine()) != null) {
                String[] parts = infraction.split(" ");
                Date time = new Date(Long.parseLong(parts[0]));
                double speed = Double.parseDouble(parts[1]);
                double traffic = Double.parseDouble(parts[2]);
                double limit = Double.parseDouble(parts[3]);
                speedData.add(new DataPoint(time, speed));
                trafficData.add(new DataPoint(time, traffic));
                limitData.add(new DataPoint(time, limit));
                numSpeeds++;
            }
            speedFile.close();

            turnFile = new BufferedReader(new InputStreamReader(getActivity().openFileInput(MainActivity.fnames[1])));
            while ((infraction = turnFile.readLine()) != null) {
                Long mstime = (Long.parseLong(infraction) / MS_PER_DAY) * MS_PER_DAY;
                Date time = new Date(mstime);
                DataPoint oldPt = turnHash.remove(mstime);
                DataPoint newPt;
                if(oldPt != null) {
                    newPt = new DataPoint(time, oldPt.getY() + 1);
                } else {
                    newPt = new DataPoint(time, 1);
                }
                turnHash.put(mstime, newPt);
                turnData.remove(oldPt);
                turnData.add(newPt);
                numTurns++;
            }
            turnFile.close();

            stopFile = new BufferedReader(new InputStreamReader(getActivity().openFileInput(MainActivity.fnames[2])));
            while ((infraction = stopFile.readLine()) != null) {
                String[] parts = infraction.split(" ");
                long time = Long.parseLong(parts[0]) % (MS_PER_DAY / 1000 / 60 / 60);
                double intensity = Double.parseDouble(parts[1]);
                DataPoint point = new DataPoint(time, intensity);
                stopData.add(point);
                numStops++;
            }
            stopFile.close();

            sleepFile = new BufferedReader(new InputStreamReader(getActivity().openFileInput(MainActivity.fnames[3])));
            while ((infraction = sleepFile.readLine()) != null) {
                Long mstime = (Long.parseLong(infraction) / MS_PER_DAY) * MS_PER_DAY;
                Date time = new Date(mstime);
                DataPoint oldPt = sleepHash.remove(mstime);
                DataPoint newPt;
                if(oldPt != null) {
                    newPt = new DataPoint(time, oldPt.getY() + 1);
                } else {
                    newPt = new DataPoint(time, 1);
                }
                sleepHash.put(mstime, newPt);
                sleepData.remove(oldPt);
                sleepData.add(newPt);
                numSleeps++;
            }
            sleepFile.close();

            accFile = new BufferedReader(new InputStreamReader(getActivity().openFileInput(MainActivity.fnames[4])));
            while ((infraction = accFile.readLine()) != null) {
                String[] parts = infraction.split(" ");
                long mstime = Long.parseLong(parts[0]) % (MS_PER_DAY / 1000 / 60 / 60);
                Date time = new Date(mstime);
                double intensity = Double.parseDouble(parts[1]);
                DataPoint point = accHash.remove(mstime);
                DataPoint newPt;
                if(point != null) {
                    newPt = new DataPoint(time, point.getY() + intensity);
                } else {
                    newPt = new DataPoint(time, intensity);
                }
                accHash.put(mstime, newPt);
                accData.remove(point);
                accData.add(newPt);
                numAccs++;
            }
            accFile.close();
        } catch (Exception e) {
            e.printStackTrace();
            return fview;
        }
        Collections.sort(speedData, new DataPointComparator());
        Collections.sort(trafficData, new DataPointComparator());
        Collections.sort(limitData, new DataPointComparator());
        Collections.sort(turnData, new DataPointComparator());
        Collections.sort(stopData, new DataPointComparator());
        Collections.sort(sleepData, new DataPointComparator());
        Collections.sort(accData, new DataPointComparator());

        DataPoint[] points = new DataPoint[speedData.size()];
        speedData.toArray(points);
        speedLine = new LineGraphSeries<DataPoint>(points);
        points = new DataPoint[trafficData.size()];
        trafficData.toArray(points);
        trafficLine = new LineGraphSeries<DataPoint>(points);
        points = new DataPoint[limitData.size()];
        limitData.toArray(points);
        limitLine = new LineGraphSeries<DataPoint>(points);

        speedLine.setDataPointsRadius(5);
        speedLine.setDrawDataPoints(true);
        speedLine.setColor(Color.BLUE);
        trafficLine.setDataPointsRadius(5);
        trafficLine.setDrawDataPoints(true);
        trafficLine.setColor(Color.RED);
        limitLine.setDataPointsRadius(5);
        limitLine.setDrawDataPoints(true);
        limitLine.setColor(Color.GREEN);

        points = new DataPoint[turnData.size()];
        turnData.toArray(points);
        sharpTurnsBar = new BarGraphSeries<DataPoint>(points);
        points = new DataPoint[stopData.size()];
        stopData.toArray(points);
        suddenStopPoints = new PointsGraphSeries<DataPoint>(points);
        points = new DataPoint[sleepData.size()];
        sleepData.toArray(points);
        sleepyLine = new LineGraphSeries<DataPoint>(points);
        points = new DataPoint[accData.size()];
        accData.toArray(points);
        highAccBar = new BarGraphSeries<DataPoint>(points);
        System.out.println(points.length);

        suddenStopPoints.setSize(15);

        graph = (GraphView) fview.findViewById(R.id.graph);
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        graph.getGridLabelRenderer().setVerticalAxisTitle("Speed (mph)");
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day");

        graph.addSeries(speedLine);

        ((TextView)fview.findViewById(R.id.turnNum)).setText("" + (numTurns-1));
        ((TextView)fview.findViewById(R.id.stopNum)).setText("" + (numStops-1));
        ((TextView)fview.findViewById(R.id.sleepNum)).setText("" + (numSleeps-1));
        ((TextView)fview.findViewById(R.id.accNum)).setText("" + (numAccs-1));
        ((TextView)fview.findViewById(R.id.drivingScoreResult)).setText(String.format("%02d", 100 - (numSpeeds + numTurns + numStops + numSleeps + numAccs)/5));
        // Inflate the layout for this fragment
        return fview;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        View wrapper = getActivity().findViewById(R.id.suddenStopWrapper);
        wrapper.setAlpha((float) 1.0);
        wrapper.setElevation((float) 20);
        wrapper = getActivity().findViewById(R.id.sharpTurnWrapper);
        wrapper.setAlpha((float) 1.0);
        wrapper.setElevation((float) 20);
        wrapper = getActivity().findViewById(R.id.sleepyConditionWrapper);
        wrapper.setAlpha((float) 1.0);
        wrapper.setElevation((float) 20);
        wrapper = getActivity().findViewById(R.id.highAccelerationWrapper);
        wrapper.setAlpha((float) 1.0);
        wrapper.setElevation((float) 20);
        graph.removeAllSeries();

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        graph.getGridLabelRenderer().setVerticalAxisTitle("Speed (mph)");
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day");
        if(((Switch)getActivity().findViewById(R.id.speed)).isChecked()) {
            graph.addSeries(speedLine);
        }
        if(((Switch)getActivity().findViewById(R.id.traffic)).isChecked()) {
            graph.addSeries(trafficLine);
        }
        if(((Switch)getActivity().findViewById(R.id.limit)).isChecked()) {
            graph.addSeries(limitLine);
        }
    }

    public void addSpeedPoint(long time, double speed, double traffic, double limit) {
        numSpeeds++;
        Date date = new Date(time);
        DataPoint sped = new DataPoint(date, speed);
        DataPoint traf = new DataPoint(date, traffic);
        DataPoint lim = new DataPoint(date, limit);
        speedLine.appendData(sped, false, numSpeeds);
        trafficLine.appendData(traf, false, numSpeeds);
        limitLine.appendData(lim, false, numSpeeds);
        updateScores();
    }

    public void addSharpTurn(long time) {
        Long mstime = (time/ MS_PER_DAY) * MS_PER_DAY;
        Date day = new Date(mstime);
        DataPoint oldPt = turnHash.remove(mstime);
        DataPoint newPt;
        if(oldPt != null) {
            newPt = new DataPoint(day, oldPt.getY() + 1);
        } else {
            newPt = new DataPoint(day, 1);
        }
        turnHash.put(mstime, newPt);
        turnData.remove(oldPt);
        turnData.add(newPt);
        numTurns++;
        Collections.sort(turnData, new DataPointComparator());
        DataPoint[] points = new DataPoint[turnData.size()];
        turnData.toArray(points);
        graph.removeSeries(sharpTurnsBar);
        sharpTurnsBar = new BarGraphSeries<DataPoint>(points);
        if(getActivity().findViewById(R.id.sharpTurnWrapper).getAlpha() == (float) 0.7) {
            graph.addSeries(sharpTurnsBar);
        }
        updateScores();
    }

    public void addSuddenStop(long time, double intensity) {
        long day = time % (MS_PER_DAY / 1000 / 60 / 60);
        DataPoint point = new DataPoint(day, intensity);
        stopData.add(point);
        numStops++;
        Collections.sort(stopData, new DataPointComparator());
        DataPoint[] points = new DataPoint[stopData.size()];
        stopData.toArray(points);
        graph.removeSeries(suddenStopPoints);
        suddenStopPoints = new PointsGraphSeries<DataPoint>(points);
        if(getActivity().findViewById(R.id.suddenStopWrapper).getAlpha() == (float) 0.7) {
            graph.addSeries(suddenStopPoints);
        }
        updateScores();
    }

    public void addSleepy(long time) {
        Long mstime = (time / MS_PER_DAY) * MS_PER_DAY;
        Date day = new Date(mstime);
        DataPoint oldPt = sleepHash.remove(mstime);
        DataPoint newPt;
        if(oldPt != null) {
            newPt = new DataPoint(day, oldPt.getY() + 1);
        } else {
            newPt = new DataPoint(day, 1);
        }
        sleepHash.put(mstime, newPt);
        sleepData.remove(oldPt);
        sleepData.add(newPt);
        numSleeps++;
        Collections.sort(sleepData, new DataPointComparator());
        DataPoint[] points = new DataPoint[sleepData.size()];
        sleepData.toArray(points);
        graph.removeSeries(sleepyLine);
        sleepyLine = new LineGraphSeries<DataPoint>(points);
        if(getActivity().findViewById(R.id.sleepyConditionWrapper).getAlpha() == (float) 0.7) {
            graph.addSeries(sleepyLine);
        }
        updateScores();
    }

    public void addHighAcc(long time, double intensity) {
        long mstime = time % (MS_PER_DAY / 1000 / 60 / 60);
        Date hour = new Date(mstime);
        DataPoint point = accHash.remove(mstime);
        DataPoint newPt = new DataPoint(hour, point.getY() + intensity);
        accHash.put(mstime, newPt);
        accData.remove(point);
        accData.add(newPt);
        numAccs++;
        Collections.sort(accData, new DataPointComparator());
        DataPoint[] points = new DataPoint[accData.size()];
        accData.toArray(points);
        graph.removeSeries(highAccBar);
        highAccBar = new BarGraphSeries<DataPoint>(points);
        if(getActivity().findViewById(R.id.highAccelerationWrapper).getAlpha() == (float) 0.7) {
            graph.addSeries(highAccBar);
        }
        updateScores();
    }

    private void updateScores() {
        ((TextView)getActivity().findViewById(R.id.turnNum)).setText("" + (numTurns-1));
        ((TextView)getActivity().findViewById(R.id.stopNum)).setText("" + (numStops-1));
        ((TextView)getActivity().findViewById(R.id.sleepNum)).setText("" + (numSleeps-1));
        ((TextView)getActivity().findViewById(R.id.accNum)).setText("" + (numAccs-1));
        ((TextView)getActivity().findViewById(R.id.drivingScoreResult)).setText(String.format("%02d", 104 - (numSpeeds + numTurns + numStops + numSleeps + numAccs)/5));
    }

    @Override
    public void onClick(View v) {
        graph.removeAllSeries();
        if(v.getId() == R.id.reset) {
            speedLine = new LineGraphSeries<DataPoint>();
            trafficLine = new LineGraphSeries<DataPoint>();
            limitLine = new LineGraphSeries<DataPoint>();
            sharpTurnsBar = new BarGraphSeries<DataPoint>();
            suddenStopPoints = new PointsGraphSeries<DataPoint>();
            sleepyLine = new LineGraphSeries<DataPoint>();
            highAccBar = new BarGraphSeries<DataPoint>();
            numAccs = 1;
            numSleeps = 1;
            numStops = 1;
            numTurns = 1;
            numSpeeds = 1;
            updateScores();
        } else {
            if (v.getAlpha() == (float) 0.7) {
                v.setAlpha((float) 1.0);
                v.setElevation((float) 20);

                graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
                graph.getGridLabelRenderer().setNumHorizontalLabels(3);
                graph.getGridLabelRenderer().setVerticalAxisTitle("Speed (mph)");
                graph.getGridLabelRenderer().setHorizontalAxisTitle("Day");

                if (((Switch) getActivity().findViewById(R.id.speed)).isChecked()) {
                    graph.addSeries(speedLine);
                }
                if (((Switch) getActivity().findViewById(R.id.traffic)).isChecked()) {
                    graph.addSeries(trafficLine);
                }
                if (((Switch) getActivity().findViewById(R.id.limit)).isChecked()) {
                    graph.addSeries(limitLine);
                }
            } else {
                View wrapper = getActivity().findViewById(R.id.suddenStopWrapper);
                wrapper.setAlpha((float) 1.0);
                wrapper.setElevation((float) 20);
                wrapper = getActivity().findViewById(R.id.sharpTurnWrapper);
                wrapper.setAlpha((float) 1.0);
                wrapper.setElevation((float) 20);
                wrapper = getActivity().findViewById(R.id.sleepyConditionWrapper);
                wrapper.setAlpha((float) 1.0);
                wrapper.setElevation((float) 20);
                wrapper = getActivity().findViewById(R.id.highAccelerationWrapper);
                wrapper.setAlpha((float) 1.0);
                wrapper.setElevation((float) 20);

                v.setAlpha((float) 0.7);
                v.setElevation((float) -20);

                switch (v.getId()) {
                    case R.id.sharpTurnWrapper:
                        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
                        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
                        graph.getGridLabelRenderer().setVerticalAxisTitle("# of times");
                        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day");
                        graph.addSeries(sharpTurnsBar);
                        break;
                    case R.id.suddenStopWrapper:
                        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter());
                        graph.getGridLabelRenderer().setNumHorizontalLabels(13);
                        graph.getGridLabelRenderer().setVerticalAxisTitle("Intensity");
                        graph.getGridLabelRenderer().setHorizontalAxisTitle("Hour of Day");
                        graph.addSeries(suddenStopPoints);
                        break;
                    case R.id.sleepyConditionWrapper:
                        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
                        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
                        graph.getGridLabelRenderer().setVerticalAxisTitle("# of times");
                        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day");
                        graph.addSeries(sleepyLine);
                        break;
                    case R.id.highAccelerationWrapper:
                        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter());
                        graph.getGridLabelRenderer().setNumHorizontalLabels(13);
                        graph.getGridLabelRenderer().setVerticalAxisTitle("Total Intensity");
                        graph.getGridLabelRenderer().setHorizontalAxisTitle("Hour of Day");
                        graph.addSeries(highAccBar);
                        break;
                    default:
                        break;
                }
            }
        }
    }



    public class DataPointComparator implements Comparator<DataPoint> {
        @Override
        public int compare(DataPoint dp1, DataPoint dp2) {
            return (int)(dp1.getX() - dp2.getX());
        }
    }
}
